# MyAnular4

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.3.2.

## Steps to setup project
1. create Angular project:
```
ng new my-anular4 --routing --style scss
```
2. install express and related dependencies:
```
npm install --save express body-parser express-jwt jwks-rsa method-override mongoose cors --save
```
3. create server.js under project root directory, copy&paste
```js
// server.js
// server.js
/*
 |--------------------------------------
 | Dependencies
 |--------------------------------------
 */
// Modules
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const methodOverride = require('method-override');
const cors = require('cors');
const config = require('./server/config');
// Config
// const config = require('./server/config');
/*
 |--------------------------------------
 | MongoDB
 |--------------------------------------
 */
/*
 |--------------------------------------
 | App
 |--------------------------------------
 */

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(cors());

// Set port
const port = process.env.PORT || '8083';
app.set('port', port);

// Set static path to Angular app in dist
// Don't run in dev
if (process.env.NODE_ENV !== 'dev') {
  app.use('/', express.static(path.join(__dirname, './dist')));
}

/*
 |--------------------------------------
 | Routes
 |--------------------------------------
 */
require('./server/api')(app, config);

// Pass routing to Angular app
// Don't run in dev
if (process.env.NODE_ENV !== 'dev') {
  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, '/dist/index.html'));
  });
}

app.listen(port, () => console.log(`Server running on localhost:${port}`));

```
4. create server folder create 2 files: (api.js & config.js leave config.js empty)
```js
// api.js
// GET API root
module.exports = function (app, config) {
  app.get('/api/', (req, res) => {
    res.send('API works');
  });
};
```
5.  check server and angular
```
ng serve
node server.js
```
6. Angular should be able to see on http://localhost:4200 and try http://localhost:8083/api should get "API works"
7. Run Angular in express
```
ng build
NODE_ENV=prod node server.js
```
8. check http://localhost:8083 should see Angular project
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
